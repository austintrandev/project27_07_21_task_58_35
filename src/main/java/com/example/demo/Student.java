package com.example.demo;

public class Student extends Person {
	public Student(int age, String gender, String name, Address address, int studentId, float averageMark) {
		super(age, gender, name, address);
		this.studentId = studentId;
		this.averageMark = averageMark;
	}

	public Student() {
		this.studentId = 1;
		this.averageMark = (float) 7.55;
	}
	private int studentId;
	private float averageMark;

	public void doHomework() {
		System.out.println("Student is doing homework!");
	}

	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public float getAverageMark() {
		return averageMark;
	}

	public void setAverageMark(float averageMark) {
		this.averageMark = averageMark;
	}

}
