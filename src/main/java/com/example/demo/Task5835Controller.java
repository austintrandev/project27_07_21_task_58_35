package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Task5835Controller {
	@CrossOrigin
	@GetMapping("/university")
	public University getUniversity() {
		
		ArrayList<Person> listPerson = new ArrayList<Person>();
		Address studentAddress = new Address("Hang Bun","Ha Noi","Viet Nam",1224);
		Address professorAddress = new Address("Nguyen Hue","HCM city","Viet Nam",1234);
		Person myPerson = new Person();
		Student myStudent = new Student(20, "female","Mary",studentAddress,1,(float) 8.5);
		Professor myProfessor = new Professor(35,"male","Peter",professorAddress,30000000);
		listPerson.add(myPerson);
		listPerson.add(myStudent);
		listPerson.add(myProfessor);
		University myUniversity = new University(15,"International Uniersity",listPerson);
		return myUniversity;
	}
}
